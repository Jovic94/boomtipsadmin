import { Component } from '@angular/core';
import { Platform, ToastController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { tap } from 'rxjs/operators';
import { TabsPage } from '../pages/tabs/tabs';
import { PredictionsProvider } from '../providers/predictions/predictions';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = TabsPage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private predictionService: PredictionsProvider, private toastCtrl: ToastController ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();

      this.predictionService.getToken();

      this.predictionService.listenToNotifications().pipe(
        tap(msg => {
          this.toastCtrl.create({
            message: msg.body,
            duration: 3000
          }).present();
        })
      ).subscribe();
    });
  }
}
