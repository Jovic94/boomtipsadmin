import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HttpModule } from "@angular/http";
import { AngularFireModule } from "angularfire2";
import { AngularFirestoreModule } from "angularfire2/firestore";

import { ViewPage } from '../pages/view/view';
import { AddPage } from '../pages/add/add';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { PredictionsProvider } from '../providers/predictions/predictions';
import { Firebase } from '@ionic-native/firebase';

const firebaseConfig = {
  apiKey: "AIzaSyCN9Ht9LU3hndCTcpP4AQo_gaYpLYfmF7w",
  authDomain: "boomtips-411ea.firebaseapp.com",
  databaseURL: "https://boomtips-411ea.firebaseio.com",
  projectId: "boomtips-411ea",
  storageBucket: "",
  messagingSenderId: "408323423135"
};

@NgModule({
  declarations: [
    MyApp,
    ViewPage,
    AddPage,
    TabsPage
  ],
  imports: [
    BrowserModule,
    AngularFirestoreModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig)

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ViewPage,
    AddPage,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    PredictionsProvider,
    Firebase
  ]
})
export class AppModule {}
