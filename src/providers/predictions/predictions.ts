import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Prediction, Inplay } from '../../shared/predictions.interface';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Platform } from 'ionic-angular';
import { Firebase } from '@ionic-native/firebase';

/*
  Generated class for the PredictionsProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class PredictionsProvider {
  predictionCollection: AngularFirestoreCollection<Prediction>;
  inplayCollection: AngularFirestoreCollection<Inplay>;

  predictions: Observable<Prediction[]>;
  inplays: Observable<Inplay[]>;

  predictionDoc: AngularFirestoreDocument<Prediction>;
  inplayDoc: AngularFirestoreDocument<Inplay>;

  currentDate: any;
  inCurrentDate: any;

  constructor(public http: Http, public afs: AngularFirestore, private platform: Platform, private firebaseNative: Firebase) {
    this.tipFunction();
    this.inplayFunction();
  }

  getPredictions() {
    return this.predictions;
  }

  addTip(tip) {
    this.predictionCollection.add(tip);
  }

  editPrediction(tip) {
    this.predictionDoc = this.afs.doc(`Predictions/${tip.id}`);
    this.predictionDoc.update(tip);
  }

  deletePrediction(tip: Prediction) {
    this.predictionDoc = this.afs.doc(`Predictions/${tip.id}`);
    this.predictionDoc.delete();
  }

  tipFunction() {
    this.currentDate = new Date().toDateString().toString();
    this.predictionCollection = this.afs.collection<Prediction>('Predictions');
    this.predictions = this.afs.collection('Predictions',
      ref => ref.where('date', '==', this.currentDate)).snapshotChanges().map(changes => {
        return changes.map(change => {
          const data = change.payload.doc.data() as Prediction;
          data.id = change.payload.doc.id;
          return data;
        });
      });
  }

  inplayFunction() {
    this.inCurrentDate = new Date().toDateString().toString();
    this.inplayCollection = this.afs.collection<Inplay>('Inplays');
    this.inplays = this.afs.collection('Inplays',
      ref => ref.where('date', '==', this.inCurrentDate)).snapshotChanges().map(changes => {
        return changes.map(change => {
          const data = change.payload.doc.data() as Inplay;
          data.id = change.payload.doc.id;
          return data;
        });
      });
  }

  getInplays() {
    return this.inplays;
  }

  addInplay(inplay) {
    this.inplayCollection.add(inplay)
  }

  editInplay(inplay) {
    this.inplayDoc = this.afs.doc(`Inplays/${inplay.id}`);
    this.inplayDoc.update(inplay);
  }

  deleteInplay(inplay: Inplay) {
    this.inplayDoc = this.afs.doc(`Inplays/${inplay.id}`);
    this.inplayDoc.delete();
  }

  // FCM'ms
  async getToken() {
    let token;

    if (this.platform.is('android')) {
      token = await this.firebaseNative.getToken();
    }

    if (this.platform.is('ios')) {
      token = await this.firebaseNative.getToken();
      await this.firebaseNative.grantPermission();
    }

    return this.saveTokenToFirestore(token);

  }

  // Save the token to firestore
  private saveTokenToFirestore(token) {
    if (!token) return;

    const devicesRef = this.afs.collection('Devices')

    const docData = {
      token
    }
    console.log(docData);

    return devicesRef.doc(token).set(docData);
  }

  // Listen to incoming FCM messages
  listenToNotifications() {
    return this.firebaseNative.onNotificationOpen()
  }

}
