import { Component } from '@angular/core';

import { ViewPage } from '../view/view';
import { AddPage } from '../add/add';

@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = ViewPage;
  tab2Root = AddPage;
  constructor() {

  }
}
