import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Prediction, Inplay } from "../../shared/predictions.interface";
import { PredictionsProvider } from '../../providers/predictions/predictions'

@Component({
  selector: 'page-add',
  templateUrl: 'add.html'
})
export class AddPage {

  prediction = {} as Prediction;
  tab: any;
  date: Date;

  inplay = {} as Inplay;

  @ViewChild('myInput') myInput: ElementRef;

  constructor(public navCtrl: NavController, private predService: PredictionsProvider) {

  }

  ionViewDidLoad() {
    this.tab = 'active'
  }

  onAddTip() {
    this.prediction.date = new Date(this.date).toDateString();
    this.prediction.result = "---";
    this.predService.addTip(this.prediction);
    this.navCtrl.setRoot(this.navCtrl.getActive().component);
  }

  onAddInplay() {
    this.inplay.date = new Date().toDateString().toString();
    this.inplay.result = '---';
    this.predService.addInplay(this.inplay);
    this.navCtrl.setRoot(this.navCtrl.getActive().component);
  }

  tab_swap(type) {
    this.tab = type;
  }

  resize() {
    var element = this.myInput['_elementRef'].nativeElement.getElementsByClassName("text-input")[0];
    var scrollHeight = element.scrollHeight;
    element.style.height = scrollHeight + 'px';
    this.myInput['_elementRef'].nativeElement.style.height = (scrollHeight + 16) + 'px';
  }

}
