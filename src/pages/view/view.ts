import { Component } from '@angular/core';
import { NavController, AlertController, LoadingController } from 'ionic-angular';
import { Prediction, Inplay } from "../../shared/predictions.interface";
import { PredictionsProvider } from '../../providers/predictions/predictions'




@Component({
  selector: 'page-view',
  templateUrl: 'view.html'
})
export class ViewPage {

  predictions: Prediction[];
  inplays: Inplay[];
  currentDate: any;
  tab: any;

  constructor(public navCtrl: NavController, private predService: PredictionsProvider, private alertCtrl: AlertController,
    private loadingCtrl: LoadingController) {

  }

  ionViewDidLoad() {
    this.tab = 'active';

    const loading = this.loadingCtrl.create({
      // content: "Kua Mpole...",
      spinner: 'bubbles',
      showBackdrop: true
    });
    loading.present();
    this.currentDate = this.predService.currentDate;
    this.predService.getPredictions().subscribe(data => {
      loading.dismiss();
      return this.predictions = data;
    });

    this.predService.getInplays().subscribe(data => {
      this.inplays = data;
      console.log(this.inplays);
    })
  }
  edit(event, tip) {
    const prompt = this.alertCtrl.create({
      title: 'Update Prediction',
      message: "Edit the result of the prediction",
      inputs: [
        {
          name: "result",
          placeholder: tip.result
        }
      ],
      buttons: [
        {
          text: "Cancel",
          handler: data => {
            console.log("Closed");
          }
        },
        {
          text: "Update",
          handler: data => {
            let newResult: string = tip.result;
            if (data.result !== '') {
              newResult = data.result;
            }
            let thetip = {
              result: newResult,
              id: tip.id,
            }
            this.predService.editPrediction(thetip);
          }
        }
      ]
    });

    prompt.present();

  }
  delete(event, tip) {
    this.predService.deletePrediction(tip);
  }

  tab_swap(type) {
    this.tab = type;
  }

}
