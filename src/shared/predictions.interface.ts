export interface Prediction {
  id?: String,
  country: String,
  league: String,
  homeTeam: String,
  awayTeam: String,
  ko: string,
  predictionTip: String,
  result: String,
  date: String
}

export interface Inplay {
  id?: String,
  inplay: String,
  date: String,
  result: String
}
