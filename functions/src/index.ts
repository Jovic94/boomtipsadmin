import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin'
admin.initializeApp();


// Send inplay Notification as the come in
export const sendNotification = functions.firestore.document('Inplays/{id}')
    .onCreate(async response => {
        const tip = response.data();
        
        const payload = {
            notification: {
                title: 'Today Inplay Tips',
                body: tip.inplay 
            }
        }

        // reference to the device collection
        const devicesRef = admin.firestore().collection('Devices');

        // Get the users tokens and send notificaitions
        const devices = await devicesRef.get();

        const tokens = [];

        // send a notification to each device token
        devices.forEach(result => {
            const token = result.data().token;

            tokens.push(token);
        });

        return admin.messaging().sendToDevice(tokens, payload).then(() => {
            console.log('Tip Sent');
        }).catch(err => console.log(err));
    });
