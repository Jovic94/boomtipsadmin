"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require("firebase-functions");
const admin = require("firebase-admin");
admin.initializeApp();
// Send inplay Notification as the come in
exports.sendNotification = functions.firestore.document('Inplays/{id}')
    .onCreate((response) => __awaiter(this, void 0, void 0, function* () {
    const tip = response.data();
    const payload = {
        notification: {
            title: 'Today Inplay Tips',
            body: tip.inplay
        }
    };
    // reference to the device collection
    const devicesRef = admin.firestore().collection('Devices');
    // Get the users tokens and send notificaitions
    const devices = yield devicesRef.get();
    const tokens = [];
    // send a notification to each device token
    devices.forEach(result => {
        const token = result.data().token;
        tokens.push(token);
    });
    return admin.messaging().sendToDevice(tokens, payload).then(() => {
        console.log('Tip Sent');
    }).catch(err => console.log(err));
}));
//# sourceMappingURL=index.js.map